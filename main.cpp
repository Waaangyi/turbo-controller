#include <sys/stat.h>

#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>

using namespace std;

string turbo_path = "/sys/devices/system/cpu/intel_pstate/no_turbo";
string temp_path = "/sys/devices/platform/coretemp.0/hwmon";
string bat_path = "/sys/class/power_supply/BAT0/status";
string override_path = "/tmp/no_turbo";

ifstream temp_file, turbo_file_r, bat_file;
ofstream turbo_file_w;
string temp_string;

int max_temp = -1;

struct stat buffer;
inline bool exists(const string& name) {
    return stat(name.c_str(), &buffer) == 0;
}

inline int read_int(ifstream& file) {
    getline(file, temp_string);
    file.seekg(0);
    return stoi(temp_string);
}

bool find_temp() {
    if (!exists(temp_path)) return false;
    bool good = 0;
    for (const auto& entry : filesystem::directory_iterator(temp_path)) {
        string p = entry.path();
        if (exists(p + "/temp1_input") && exists(p + "/temp1_max")) {
            ifstream max_file;
            max_file.open(p + "/temp1_max", ios::in);
            max_temp = read_int(max_file) / 1000;
            temp_path = p + "/temp1_input";
            good = 1;
            break;
        }
    }
    return good;
}

inline bool get_bat() {
    if (!exists(bat_path)) return 0;
    getline(bat_file, temp_string);
    bat_file.seekg(0);
    if (temp_string == "Discharging") return 1;
    return 0;
}

inline bool get_turbo() { return !read_int(turbo_file_r); }

inline int get_temperature() {
    if (!exists(temp_path)) return 1000;
    return read_int(temp_file) / 1000;
}

inline void set_turbo(const bool& state) {
    turbo_file_w.open(turbo_path, ios::out);
    if (state) {
        turbo_file_w << "0";
    } else {
        turbo_file_w << "1";
    }
    turbo_file_w.close();
}

int main() {
    if (!find_temp()) {
        cout << "Unable to find temperature file, quitting!" << endl;
        return 1;
    }
    cout << "Temperature file: " << temp_path << endl;
    cout << "Temperature threshold: " << max_temp << endl;
    temp_file.open(temp_path, ios::in);
    turbo_file_r.open(turbo_path, ios::in);
    bat_file.open(bat_path, ios::in);
    int temperature = get_temperature();
    bool turbo = get_turbo();
    while (true) {
        this_thread::sleep_for(chrono::seconds(5));
        turbo = get_turbo();
        if (exists(override_path) || get_bat()) {
            if (turbo) {
                set_turbo(false);
                system("killall -USR1 i3status");
            }
        } else {
            temperature = get_temperature();
            if (temperature <= max_temp - 10 && !turbo) {
                set_turbo(true);
                system("killall -USR1 i3status");
            }
            if (temperature >= max_temp && turbo) {
                set_turbo(false);
                system("killall -USR1 i3status");
            }
        }
    }
}
